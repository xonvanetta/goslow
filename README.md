# Goslow or go home
Made to test timeouts and how to handle those things.

## To get goslow
`go get gitlab.com/xonvanetta/goslow`

## Run goslow
`goslow`

## Command flags

Port for the webserver.

Sleep is in seconds. 

```
  -port
        Change value of Port. (default 8080)
  -sleep
        Change value of Sleep. (default 11)

Generated environment variables:
   CONFIG_PORT
   CONFIG_SLEEP
```

`goslow -port 3001 -sleep 60`
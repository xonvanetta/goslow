package main

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/koding/multiconfig"
)

type config struct {
	Port  string `default:"8080"`
	Sleep int    `default:"11"`
}

func main() {
	config := config{}
	multiconfig.New().MustLoad(&config)

	r := gin.Default()
	r.GET("/goslow", response(config))
	r.POST("/goslow", response(config))
	r.PUT("/goslow", response(config))
	r.DELETE("/goslow", response(config))

	r.Run(":" + config.Port)
}

func response(c config) gin.HandlerFunc {
	return func(g *gin.Context) {
		time.Sleep(time.Second * time.Duration(c.Sleep))
		g.Status(http.StatusOK)
	}
}
